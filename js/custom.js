$(function () {
	var win 		= $(window),
		doc			= $(document),
		backSlider 	= doc.find("#backSlider"),
		aboutIcon	= doc.find("#aboutIcon"),
		nav			= doc.find("#mainNav ul"),
		pos;

	win.scroll(function () {
		pos = win.scrollTop();
		backSlider.toggleClass("fixed", pos > 450 && pos < 1100);		
		aboutIcon.toggleClass("fixed", pos > 1100);	
		nav.toggleClass("fixed", pos > 450);
	})
});